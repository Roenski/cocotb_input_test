# cocotb_input_test

Shows the functionality of providing input values for a testbench DURING the simulation in cocotb

Prerequisites:
* cocotb (https://docs.cocotb.org/en/stable/install.html)
* ghdl: the version used here was 3.0.0-dev (install with e.g. `apt` or build from source (mcode version is enough, see the TL;DR section): https://ghdl.github.io/ghdl/development/building/index.html#build)

Run with the following command:
```make SIM=ghdl```


