import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge

@cocotb.test()
async def moving_average_test(dut):
    cocotb.start_soon(
            Clock(dut.i_clk, 10, units="ns").start())
    dut.i_rstb.value = 1
    dut.i_sync_reset.value = 1
    dut.i_data_ena.value = 0
    dut.i_data.value = 0

    # Reset procedure
    await RisingEdge(dut.i_clk)
    await RisingEdge(dut.i_clk)
    dut.i_sync_reset.value = 0
    await RisingEdge(dut.i_clk)
    await RisingEdge(dut.i_clk)
    dut._log.info(dut.o_data_valid)
    dut._log.info(dut.o_data)

    assert dut.o_data_valid == 0, f"Reset didnt work"
    assert dut.o_data == 0, f"Reset didnt work"

    for i in range(0,100):
        dut.i_data.value = \
                int(input("Provide input value: "))
        dut.i_data_ena.value = 1
        await RisingEdge(dut.i_clk)
        dut._log.info(
                "Output data: " + \
                        str(int(dut.o_data.value)))

